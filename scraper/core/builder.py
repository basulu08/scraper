class ExtractorDict:
    def __init__(self, dom):
        self.dom = dom
        self._contents = {}

    def _get_attr(self, selector: str, target: str):
        return self.dom.cssselect(selector)[0].get(target)

    def _get_text(self, selector: str):
        return self.dom.cssselect(selector)[0].text

    def _get_elem(self, selector: str, target: str = None, func: object = None):
        result = self._get_text(selector) if target is None else self._get_attr(selector, target)
        return result if func is None else func(result)

    def __getitem__(self, key):
        return self._contents[key]

    def __setitem__(self, key, value):
        self._contents[key] = self._get_elem(**value)

    @property
    def content(self):
        return self._contents

    @content.setter
    def content(self, value: tuple):
        for key, val in value:
            self.__setitem__(key, val)
