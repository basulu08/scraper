from abc import ABCMeta, abstractmethod


class BaseSite(metaclass=ABCMeta):
    def __init__(self, main_selector):
        self.main_selector = main_selector
        self._list_extractor = None
        self._detail_extractor = None

    @staticmethod
    def _check_type(value, dtype):
        if type(value) is not dtype:
            raise TypeError('適切な値ではありません:', value, dtype)

    @staticmethod
    def _gen_extractor(extractor: dict):
        for key, ext in extractor.items():
            if type(ext) is not dict:
                print('設定方法が違います')
                continue

            if 'selector' not in ext:
                print('不正な設定です')
                continue

            yield key, ext

    @property
    def list_extractor(self):
        return self._list_extractor

    @list_extractor.setter
    def list_extractor(self, value):
        self._check_type(value, dict)
        self._list_extractor = value

    def add_list_extractor(self, key: str, selector: str, target: str = None, func: callable = None):
        self._list_extractor.append([key, selector, target, func])

    @property
    def detail_extractor(self):
        return self._detail_extractor

    @detail_extractor.setter
    def detail_extractor(self, value):
        self._check_type(value, dict)
        self._detail_extractor = value

    def add_detail_extractor(self, key: str, selector: str, target: str = None, func: callable = None):
        self._list_extractor.append([key, selector, target, func])
