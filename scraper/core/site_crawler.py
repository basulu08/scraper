import time
import requests
import lxml.html
from lxml import etree

from schematics.exceptions import DataError, ValidationError
from core.error_handling import test_fetch
from core.builder import ExtractorDict
from core.base_site import BaseSite
from core.utils import BaseUtil
import traceback


class SiteCrawler:
    def __init__(self, site: BaseSite, util: BaseUtil):
        self.site = site
        self.util = util
        self.debug = False
        self.cookies = {'HEXAVID_LOGIN': 'bbbc7f5bc3682d4cQclXl7VTlD-wFZR2tIFlAU3odHNFs9SogXjS_muxzrb58RBEE0zsWVRRHk0X0pYwUoX4nidtA6UR20BwbRme5odcCZf1MRq0auj8hWRcwGtnIhDZ_-XTBOt6f5ojfb45eCWwTiFIdcC7ySc3Av-ainVtMYWlI4MJhTa5XpmaXtj9m1lggjzg79r9cJftKOa9bkXYGBNUSH_p1W1b066qTw%3D%3D'}

    def scrape_listitem(self, response: object, key, ext):
        listitem = self.scrape_list_page(response, ext['selector'], {'listitem': ext['listitem']})
        listitem = [item['listitem'] for item in listitem]
        return {key: '' if len(listitem) == 0 else ','.join(listitem)}

    def scrape_detail_page(self, response: object, extractor: dict):
        root = lxml.html.fromstring(response.content.decode('utf-8'))
        ext_dict = ExtractorDict(root)
        ret = {}
        for key, ext in extractor.items():
            if 'listitem' in ext:
                ret.update(self.scrape_listitem(response, key, ext))
                continue

            if self.debug: print(key, ext)

            ext_dict[key] = ext

        ret.update(ext_dict.content)
        return ret

    def scrape_list_page(self, response: object, selector: str, extractor: dict):
        root = lxml.html.fromstring(response.content.decode('utf-8'))
        root.make_links_absolute(response.url)

        for video in root.cssselect(selector):

            ext_dict = ExtractorDict(video)
            for key, ext in extractor.items():
                try:
                    if self.debug: print(key, ext)
                    ext_dict[key] = ext

                except Exception as e:
                    traceback.print_exc()
                    print()
                    continue

            yield ext_dict.content

    def scrape_one_page(self, session: object, uri: str, delay: int = 1):
        test_fetch(uri)
        response = session.get(uri, cookies=self.cookies)

        for item in self.scrape_list_page(response, self.site.main_selector, self.site.list_extractor):

            time.sleep(delay)
            try:
                response = session.get(item['url'], cookies=self.cookies)

                detail_page = self.scrape_detail_page(response, self.site.detail_extractor)

                item.update(detail_page)

                self.util.saver(item)

            except Exception as e:
                traceback.print_exc()
                print()
                continue

    def run_only_details(self, urls: str, delay: int):
        session = requests.Session()

        for url in urls:
            try:
                test_fetch(url)

                time.sleep(1)
                response = session.get(url, cookies=self.cookies)

                item = self.scrape_detail_page(response, self.site.detail_extractor)
                item['url'] = url

                self.util.saver(item)
            except Exception as e:
                traceback.print_exc()
                print()
                continue

    def run(self, start: int, end: int, delay: int = 1):
        session = requests.Session()
        base_urls = [
            # Redtube 1 ~ 5
            # 'https://www.redtube.com/mostviewed',
            # 'https://www.redtube.com/mostviewed?period=alltime',
            # 'https://www.redtube.com/mostviewed?period=monthly',
            # 'https://www.redtube.com/top',
            # 'https://www.redtube.com/top?period=alltime',
            # 'https://www.redtube.com/top?period=monthly',

            # Xvideos actress P1~3
            # 'https://www.xvideos.com/?k=篠田ゆう&p=',
            # 'https://www.xvideos.com/?k=白石マリエ&p=',
            # 'https://www.xvideos.com/?k=Riho&p=',
            # 'https://www.xvideos.com/?k=麻生希&p=',
            # 'https://www.xvideos.com/?k=立花里子&p=',
            # 'https://www.xvideos.com/?k=篠田あゆみ&p=',
            # 'https://www.xvideos.com/?k=麻美ゆま&p=',
            # 'https://www.xvideos.com/?k=波多野結衣&p=',
            # 'https://www.xvideos.com/?k=川上奈々美&p=',
            # 'https://www.xvideos.com/?k=沖田杏梨&p=',
            # 'https://www.xvideos.com/?k=田中ひとみ&p=',
            # 'https://www.xvideos.com/?k=明日花キララ&p=',

            # Xvideos Japanese bests Y2018~2015, P1~5
            # Xvideos Japanese bests Y2018~2017, P1~10
            # Xvideos american bests Y2018~2017, P1~5
            # 'https://www.xvideos.com/best/2017-12',
            # 'https://www.xvideos.com/best/2017-11',
            # 'https://www.xvideos.com/best/2017-10',
            # 'https://www.xvideos.com/best/2017-09',
            # 'https://www.xvideos.com/best/2017-08',
            # 'https://www.xvideos.com/best/2017-07',
            # 'https://www.xvideos.com/best/2017-06',
            # 'https://www.xvideos.com/best/2017-05',
            # 'https://www.xvideos.com/best/2017-04',
            # 'https://www.xvideos.com/best/2017-03',
            # 'https://www.xvideos.com/best/2017-02',
            # 'https://www.xvideos.com/best/2017-01',

            # Xvideos Korean,
            # 'https://www.xvideos.com/?k=Forss008+&p=',
            # 'https://www.xvideos.com/?k=korean+scandal&p=',
            # 'https://www.xvideos.com/?k=RUSSIAN+DESIRE&p=',
            'https://www.xvideos.com/?k=Takevan&p=',
            # 'https://www.xvideos.com/?k=Batisfarka&p=',
            # 'https://www.xvideos.com/?k=Net+Video+Girls+&p=',
        ]

        for base_url in base_urls:
            self.util.BASE_URL = base_url

            for i in range(start, end):
                uri = self.util.get_page(i)
                self.scrape_one_page(session, uri, delay)
