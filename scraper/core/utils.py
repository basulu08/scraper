import re
from abc import ABCMeta, abstractmethod


def ask_yn():
    yn_dict = {'y': True, 'yes': True, 'n': False, 'no': False}
    while True:
        inp = input('[Y]es/[N]o? >> ').lower()
        if inp in yn_dict:
            inp = yn_dict[inp]
            break
        print('Error! Input again.')

    return inp


def normalize_spaces(s: str):
    return re.sub(r'\s+', '', s).strip()


class BaseUtil(metaclass=ABCMeta):
    NAME = ''
    BASE_URL = ''

    def __init__(self, target):
        self.target = target
        self.sid = None

    @abstractmethod
    def get_page(self, n: int) -> int:
        pass

    @abstractmethod
    def saver(self, item: dict):
        pass

