
import re
from core.models import *
import traceback
from lxml import etree
import datetime

# 共通
HOSTNAME = 'https://shicorotto.mixh.jp/'
XML_TAG = '<?xml version="1.0" encoding="UTF-8"?>\n'

# サイトマップ(ページ)
SITEMAP_PAGES_FILE_NAME = 'sitemap_pages.xml'
PAGE_URL = '?page='
END_PAGE = 568
OTHER_PAGE_PATHS = ['', 'slot', 'search']

# サイトマップ（詳細）
SITEMAP_VIDEOS_FILE_NAME = 'sitemap_videos.xml'
WATCH_URL = 'videos/'

# インデックスファイル
SITEMAPINDEX_FILE_NAME = 'sitemapindex.xml'
XML_FILES = ['sitemap_pages.xml', 'sitemap_videos.xml']


def gen_sitemap_videos():
    urlset = etree.Element('urlset', xmlns='http://www.sitemaps.org/schemas/sitemap/0.9')

    for video in Videos.select():
        try:
            url = etree.SubElement(urlset, 'url')

            loc = etree.SubElement(url, 'loc')
            loc.text = f'{HOSTNAME}{WATCH_URL}{video.key}'

            lastmod = etree.SubElement(url, 'lastmod')
            lastmod.text = video.updated_at.strftime('%Y-%m-%d')

            changefreq = etree.SubElement(url, 'changefreq')
            changefreq.text = 'weekly'


        except Exception as e:
            traceback.print_exc()
            print('==== traceback ====')
            continue

    with open(SITEMAP_VIDEOS_FILE_NAME, mode='w') as f:
        f.write(XML_TAG + etree.tostring(urlset, pretty_print=True).decode('utf-8'))

def gen_sitemap_pages():
    urlset = etree.Element('urlset', xmlns='http://www.sitemaps.org/schemas/sitemap/0.9')

    # その他のページURLタグ
    for path in OTHER_PAGE_PATHS:
        url = etree.SubElement(urlset, 'url')

        loc = etree.SubElement(url, 'loc')
        loc.text = f'{HOSTNAME}{path}'

        lastmod = etree.SubElement(url, 'lastmod')
        lastmod.text = datetime.datetime.today().strftime('%Y-%m-%d')

        changefreq = etree.SubElement(url, 'changefreq')
        changefreq.text = 'daily'

    # 各ページのURLタグ
    for i in range(1, END_PAGE + 1):
        try:
            url = etree.SubElement(urlset, 'url')

            loc = etree.SubElement(url, 'loc')
            loc.text = f'{HOSTNAME}{PAGE_URL}' + str(i)

            lastmod = etree.SubElement(url, 'lastmod')
            lastmod.text = datetime.datetime.today().strftime('%Y-%m-%d')

            changefreq = etree.SubElement(url, 'changefreq')
            changefreq.text = 'daily'


        except Exception as e:
            traceback.print_exc()
            print('==== traceback ====')
            continue

    with open(SITEMAP_PAGES_FILE_NAME, mode='w') as f:
        f.write(XML_TAG + etree.tostring(urlset, pretty_print=True).decode('utf-8'))


def gen_sitemapindex():
    sitemapindex = etree.Element('sitemapindex', xmlns='http://www.sitemaps.org/schemas/sitemap/0.9')

    for xml_file in XML_FILES:
        try:
            sitemap = etree.SubElement(sitemapindex, 'sitemap')

            loc = etree.SubElement(sitemap, 'loc')
            loc.text = f'{HOSTNAME}{xml_file}'

            lastmod = etree.SubElement(sitemap, 'lastmod')
            lastmod.text = datetime.datetime.today().strftime('%Y-%m-%d')


        except Exception as e:
            traceback.print_exc()
            print('==== traceback ====')
            continue

    with open(SITEMAPINDEX_FILE_NAME, mode='w') as f:
        f.write(XML_TAG + etree.tostring(sitemapindex, pretty_print=True).decode('utf-8'))


if __name__ == '__main__':
    gen_sitemap_pages()
    gen_sitemap_videos()
    gen_sitemapindex()