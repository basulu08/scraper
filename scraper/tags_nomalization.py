
import re
from core.models import *
from hashids import Hashids
import traceback
import pickle

STARTSWITH_DUPLICATIONS = {
    'Korea': ['kore'],
    # 'Japan': ['jap'],
    'China': ['chine', 'china'],
    # 'USA': ['ameri', 'usa'],
    'Germany': ['germ'],
    'Russia': ['russi'],
    'Italy': ['itali']
}

def videos_tags_search_and_replace(target_id, new_id):
    videos_tags = VideosTags.select().where(VideosTags.tag_id == target_id)

    for videos_tag in videos_tags:
        videos_tag.tag_id = new_id
        videos_tag.save()


def tag_search_and_replace(tag, likes):
    original_tag = Tags.get(Tags.tag == tag)
    original_tag.tag = tag

    for like in likes:
        matches = Tags.select().where(Tags.tag.startswith(like), Tags.tag != tag)

        for match in matches:
            videos_tags_search_and_replace(match.id, original_tag.id)
            match.delete_instance()
    
    original_tag.save()


def nomalize(method):
    if method == 'startswith':
        for tag, likes in STARTSWITH_DUPLICATIONS.items():
            tag_search_and_replace(tag, likes)
    else:
        print('一致する方式がありませんでした')


def main():
    with open('slang_dict.pickle', mode='rb') as f:
        slang_dict = pickle.load(f)

    with open('region_dict.pickle', mode='rb') as f:
        region_dict = pickle.load(f)

    for tag in Tags.select():
        require_save = False

        try:
            tag_name = tag.tag.capitalize()

            if tag_name in slang_dict:
                tag.jap = slang_dict.get(tag_name, tag_name)
                require_save = True
                print(tag_name)

            if tag_name in region_dict:
                tag.jap = region_dict.get(tag_name, tag_name)
                tag.tag = tag_name
                require_save = True
                print(tag_name)

            if require_save:
                tag.save()

        except Exception as e:
            traceback.print_exc()
            print('==== traceback ====')
            continue


if __name__ == '__main__':
    # main()
    nomalize('startswith')