
import re
import pickle
from core import validators
from core.models import *
from core.utils import BaseUtil
from peewee import DoesNotExist
from hashids import Hashids


def insert_videos(item: dict):
    vv = validators.VideoValidator()
    vv.title = item.get('title', '')
    vv.url = item.get('url', '')
    vv.key = item.get('key', '')
    vv.img = item.get('img', '')
    vv.thumb = item.get('thumb', '')
    vv.duration = item.get('duration', '')
    vv.views = item.get('views', '')
    vv.embed = item.get('embed', '')
    vv.rate = item.get('rate', '')
    vv.site_id = item['site_id']
    vv.validate()
    try:
        video = Videos.get(Videos.key == vv.key)
    except DoesNotExist:
        video = Videos.create(**vv.to_primitive())

    return video.id

def insert_tags(tag: str, jap: str, is_region: int) -> int:
    tag_validator = validators.TagValidator()
    tag_validator.tag = tag
    tag_validator.jap = jap
    tag_validator.is_region = is_region
    tag_validator.validate()

    try:
        tag = Tags.get(Tags.tag == tag_validator.tag)
    except DoesNotExist:
        tag = Tags.create(tag=tag, jap=jap, is_region=is_region)

    return tag.id


def insert_videos_tags(video_id: int, tag_id: int) -> int:
    record, created = VideosTags.get_or_create(video_id=video_id, tag_id=tag_id)
    return record.id


def insert_sites(site: str) -> int:
    record, created = Sites.get_or_create(site=site)
    return record.id


class XvideosUtil(BaseUtil):
    NAME = 'xvideos'
    # BASE_URL = 'https://www.xvideos.com'
    BASE_URL = 'https://www.xvideos.com/best/2018-10'
    COND = 's:views/m:6month/'
    IS_REGION = 0

    # italy
    REGION_ID = 7169

    def __init__(self, target):
        super().__init__(target)
        with database.atomic():
            site, created = Sites.get_or_create(site=self.NAME)

        self.sid = site.id

        with open('slang_dict.pickle', mode='rb') as f:
            self.slang_dict = pickle.load(f)

    def get_page(self, n: int) -> str:
        tags_pos = self.BASE_URL.find('tags')

        if n == 0:
            if tags_pos != -1:
                return f'{self.BASE_URL}/{self.COND}'
            else:
                return self.BASE_URL

        if re.search(r'https:\/\/www\.xvideos\.com$', self.BASE_URL):
            return f'{self.BASE_URL}/new/{n}'

        elif tags_pos != -1:
            return f'{self.BASE_URL}/{n}/{self.COND}'

        elif self.BASE_URL.find('best') != -1:
            return f'{self.BASE_URL}/{n}'

        elif self.BASE_URL.find('&p=') != -1:
            return f'{self.BASE_URL}{n}'


    def extract_key(self, url: str) -> str:
        matches = re.search(r'www\.xvideos\.com\/(.+?)\/', url)
        key = matches.group(1)
        hashids = Hashids(salt=self.NAME, min_length=16)
        if key.find('video') != -1:
            key = re.search(r'^video(\d+)', key).group(1)
            key = list(map(lambda v: int(v), list(key)))
            key = hashids.encode(*key)

        return key

    def saver(self, item: dict):

        with database.atomic():
            if self.target == 'video':
                item = {key: item[key] for key in item if item[key] is not None}

                print(item, '\n')
                item['key'] = self.extract_key(item['url'])
                item['site_id'] = self.sid
                item['video_id'] = insert_videos(item)

                tags = item.get('tags', '')
                tags = tags.split(',') if tags is not '' else []

                tag_ids = []
                for tag in tags:
                    jap = self.slang_dict.get(tag, tag)
                    tag_ids.append(insert_tags(tag, jap, self.IS_REGION))

                for tag_id in tag_ids:
                    insert_videos_tags(item['video_id'], tag_id)
                    insert_videos_tags(item['video_id'], self.REGION_ID)

            elif self.target == 'user':
                pass
            else:
                raise ValueError('対応した保存先が存在しません')

