from core.site_factory import SiteFactory
import re
from toolz import functoolz as fz

from core.utils import normalize_spaces
from xvideos.util import XvideosUtil

def extract_time_from_description(text: str) -> str:
    d = re.search(r'\((\d*:?\d*:?\d\d)\)', text)
    return d.group(1)

def extract_iframe_src(text: str) -> str:
    d = re.search(r'<iframe src=[\"\'](http.+?)[\"\']', text)
    return d.group(1)


def extract_time(text: str) -> str:
    d = re.search(r'\d*:?\d*:?\d\d', text)
    return d.group(0)

def extract_duration(text: str) ->str:
    matches = re.findall('(\d+)\s?[h|m|s]', text)
    return ':'.join(matches)

class XvideosFactory(SiteFactory):
    def get_main_selector(self):
        if self.detail_only:
            return ''

        if self.target == 'video':
            return '.mozaique > .thumb-block'
        else:
            raise ValueError('スクレイプに対応していない要素です')

    def get_list_extractor(self):
        if self.detail_only:
            return {}

        if self.target == 'video':
            return {
                'url': {'selector': '.thumb > a', 'target': 'href'},
                'img': {'selector': '.thumb > a > img', 'target': 'src'},
                # 'duration': {'selector': 'span.bg > span.duration', 'func': extract_time},
                'duration': {'selector': 'span.bg > span.duration'},
            }
        else:
            return {}

    def get_detail_extractor(self, detail_only=False):
        if self.target == 'video':
            extractor = {
                'title': {'selector': '#main > h2.page-title', 'func': lambda v: v.strip()},
                'views': {'selector': '#nb-views-number'},
                # リストページからうまく取得できないため詳細から取得
                'img': {'selector': 'meta[property="og:image"]', 'target': 'content'},
                'embed': {'selector': '#copy-video-embed', 'target': 'value', 'func': extract_iframe_src},
                'tags': {
                    'selector': '.video-metadata > ul > li > a[href*="/tags/"]',
                    'listitem': {'selector': 'a'}
                }
            }
            if self.detail_only:
                extractor.update({
                    'img': {'selector': 'meta[property="og:image"]', 'target': 'content'},
                    'duration': {'selector': 'h2.page-title > span.duration'},
                })

            return extractor
        else:
            return {}

    def get_util(self):
        return XvideosUtil(self.target)
