
import re
from core.models import *
import traceback
from core.error_handling import test_fetch
import requests
import lxml.html
import pickle

URL = 'https://yattoke.com/2018/04/11/world-country/'
ENG_SELECTOR = '.basic > tbody > tr > td:first-child > b'
JAP_SELECTOR = '.basic > tbody > tr > td:last-child > b'

def main():
    test_fetch(URL)

    session = requests.Session()
    response = session.get(URL)
    root = lxml.html.fromstring(response.content.decode('utf-8'))

    eng = root.cssselect(ENG_SELECTOR)
    jap = root.cssselect(JAP_SELECTOR)

    d = []
    for i in range(len(eng)):
        english = eng[i].text.replace(u"\xa0", u"")
        d.append(english)

    print(d)

    # with open('slang_dict.pickle', mode='rb') as f:
    #     slang_dict = pickle.load(f)

    # for i in range(len(eng)):
    #     english = eng[i].text.replace(u"\xa0", u"")
    #     del slang_dict[english]
    #     # slang_dict[english] = jap[i].text

    # with open('slang_dict.pickle', mode='wb') as f:
    #     pickle.dump(slang_dict, f)
    
    # print(slang_dict)


if __name__ == "__main__":
    main()
    
