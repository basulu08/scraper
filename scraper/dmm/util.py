
import re
from core import validators
from core.models import *
from core.utils import BaseUtil
from peewee import DoesNotExist


def insert_videos(item: dict):
    vv = validators.VideoValidator()
    vv.title = item.get('title', '')
    vv.url = item.get('url', '')
    vv.key = item.get('key', '')
    vv.img = item.get('img', '')
    vv.thumb = item.get('thumb', '')
    vv.duration = item.get('duration', '')
    vv.views = item.get('views', '')
    vv.embed = item.get('embed', '')
    vv.rate = item.get('rate', '')
    vv.site_id = item['site_id']
    vv.validate()
    try:
        video = Videos.get(Videos.key == vv.key, Videos.site == vv.site_id)
    except DoesNotExist:
        video = Videos.create(**vv.to_primitive())

    return video.id

def insert_tags(tag: str) -> int:
    tag_validator = validators.TagValidator()
    tag_validator.tag = tag
    tag_validator.validate()

    tag, created = Tags.get_or_create(tag=tag)
    return tag.id


def insert_videos_tags(video_id: int, tag_id: int) -> int:
    record, created = VideosTags.get_or_create(video_id=video_id, tag_id=tag_id)
    return record.id


def insert_sites(site: str) -> int:
    record, created = Sites.get_or_create(site=site)
    return record.id


class RedtubeUtil(BaseUtil):
    NAME = 'redtube'
    BASE_URL = 'https://www.redtube.com/top?search=japanese+hd'

    def __init__(self, target):
        super().__init__(target)
        with database.atomic():
            site, created = Sites.get_or_create(site=self.NAME)

        self.sid = site.id

    @classmethod
    def get_page(cls, n: int) -> str:
        return f'{cls.BASE_URL}&page={n}'

    @staticmethod
    def extract_key(url: str) -> str:
        matches = re.search(r'/([^/]+)$', url)
        return matches.group(1)

    def saver(self, item: dict):

        with database.atomic():
            if self.target == 'video':
                item = {key: item[key] for key in item if item[key] is not None}

                print(item, '\n')
                item['key'] = self.extract_key(item['url'])
                item['site_id'] = self.sid
                item['video_id'] = insert_videos(item)

                tags = item.get('tags', '')
                tags = tags.split(',') if tags is not '' else []

                for tag_id in [insert_tags(tag) for tag in tags]:
                    insert_videos_tags(item['video_id'], tag_id)

            elif self.target == 'user':
                pass
            else:
                raise ValueError('対応した保存先が存在しません')

