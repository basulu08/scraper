from core.site_factory import SiteFactory
import re
from toolz import functoolz as fz

from core.utils import normalize_spaces
from redtube.util import RedtubeUtil

def extract_time_from_description(text: str) -> str:
    d = re.search(r'\((\d*:?\d*:?\d\d)\)', text)
    return d.group(1)

def extract_iframe_src(text: str) -> str:
    d = re.search(r'<iframe src=[\"\'](http.+?)[\"\']', text)
    return d.group(1)


def extract_time(text: str) -> str:
    d = re.search(r'\d*:?\d*:?\d', text)
    return d.group(0)


class RedtubeFactory(SiteFactory):
    def get_main_selector(self):
        if self.detail_only:
            return ''

        if self.target == 'video':
            return '.videoblock_list'
        else:
            raise ValueError('スクレイプに対応していない要素です')

    def get_list_extractor(self):
        if self.detail_only:
            return {}

        if self.target == 'video':
            extract_duration = fz.compose(extract_time, normalize_spaces)
            return {
                'url': {'selector': 'a.video_link', 'target': 'href'},
                'img': {'selector': 'img.img_video_list', 'target': 'data-thumb_url'},
                'thumb': {'selector': 'img.img_video_list', 'target': 'data-mediabook'},
                # 'duration': {'selector': 'a.video_link span.duration', 'func': extract_duration},
            }
        else:
            return {}

    def get_detail_extractor(self, detail_only=False):
        if self.target == 'video':
            extractor = {
                'title': {'selector': '.video_title_text'},
                'rate': {'selector': '.rating_percent', 'func': normalize_spaces},
                'views': {'selector': 'span.video_count'},
                'embed': {'selector': 'textarea#js_video_share_embed', 'func': extract_iframe_src},
                'tags': {
                    'selector': '#video-infobox-wrap > div:nth-child(1) > div:nth-child(4) > div.video-infobox-content a',
                    'listitem': {'selector': 'a', 'func': normalize_spaces}
                },
                'duration': {
                    'selector': 'meta[property="og:description"]',
                    'target': 'content',
                    'func': extract_time_from_description
                }
            }
            if self.detail_only:
                extractor.update({
                    'img': {'selector': 'meta[property="og:image"]', 'target': 'content'},
                    'duration': {
                        'selector': 'meta[property="og:description"]',
                        'target': 'content',
                        'func': extract_time_from_description
                    },
                })

            return extractor
        else:
            return {}

    def get_util(self):
        return RedtubeUtil(self.target)
