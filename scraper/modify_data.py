
import re
from core.models import *
from hashids import Hashids
import traceback
import pickle

def extract_duration(text: str) ->str:
    matches = re.findall('(\d+)\s?[h|m|s]', text)
    return ':'.join(matches)

def main():
    for video in Videos.select():
        try:
            duration = extract_duration(video.duration)
            if len(duration) != 0:
                print(duration, video.duration)

            # video.save()

        except Exception as e:
            traceback.print_exc()
            print('==== traceback ====')
            continue


if __name__ == '__main__':
    main()