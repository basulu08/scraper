
import re
from core.models import *
import traceback
from core.error_handling import test_fetch
import requests
import lxml.html
import pickle

URL = 'http://av-sexvideos.com/xvideo_tag'
ENG_SELECTOR = '.accordion-inner.accordion-tags > .row > .tags-children:first-child > a'
JAP_SELECTOR = '.accordion-inner.accordion-tags > .row > .tags-children:last-child > a'

def main():
    test_fetch(URL)

    session = requests.Session()
    response = session.get(URL)
    root = lxml.html.fromstring(response.content.decode('utf-8'))

    slang_dict = {}
    eng = root.cssselect(ENG_SELECTOR)
    jap = root.cssselect(JAP_SELECTOR)

    for i in range(len(eng)):
        slang_dict[eng[i].text] = jap[i].text

    with open('slang_dict.pickle', mode='wb') as f:
        pickle.dump(slang_dict, f)


if __name__ == "__main__":
    main()
    
