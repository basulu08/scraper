
import re
from core.models import *
from hashids import Hashids
import traceback
import pickle

def main():
    delete_target_keys = [
        'JVc8F2fvfvSVh8f6'
    ]

    for video in Videos.select().where(Videos.key << delete_target_keys):
        video.delete_instance()


if __name__ == '__main__':
    main()