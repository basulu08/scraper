
from setuptools import setup


setup(
    name="evscraper",
    author='syarig',
    author_email='syarig110@yahoo.co.jp',
    maintainer='syarig',
    maintainer_email='syarig110@yahoo.co.jp',
    packages=['scraper'],
    version="0.1",
    test_suite='tests',
    license="GNU"
)